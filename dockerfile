#Imagen Inicial
FROM node
#Definimos e directorio del contenedor
WORKDIR /colapi_conchapi
#DSe añade el contenido del proyect en directorio de contenedor
ADD . /colapi_conchapi
#puerto de escucha
EXPOSE 3000
#comandos para lanza nuestra dAPI REST 'colapi'
CMD ["npm","start"]
