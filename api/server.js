/*@camiloconcha*/
var express      = require('express');
var app          = express();
var requestJSON  = require('request-json');
var bodyParser   = require('body-parser');
var port         = process.env.PORT || 3000;

//var routes = require('./api/routes/routes.js');


var URLbase = '/colapi/v1/'
var httpClient = requestJSON.createClient(URLbase);
var mLab  = "https://api.mlab.com/api/1/databases/colapi_concha/collections/";
var apiKey   = "&apiKey=nfEW6UViBlj7KrQ1DNE8H98e7l772mxW";

app.listen(port);
//Parse JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//End Parse JSON

//Users

app.get(URLbase+'users',function(req, res){
      httpClient.get(mLab+'user?'+apiKey,function(err,response, body){
         res.send(body);
    });
});
//Users
app.post(URLbase+'users', function(req, res) {  
    var userData = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password,
        "activeSession" : req.body.activeSession,
    }
      httpClient.post(mLab + 'user?' + apiKey, userData, function(err, res, body){});
      res.statusCode = 200 ? res.send({"msj":"OK","code":"200"}) : res.send({"msj":"FAILED","code":"404"});
});

//User
app.get(URLbase+'users',function(req, res){
  httpClient.get(mLab+'user?'+apiKey,function(err,respuestaMLab, body){
    var respuesta  = body;
    res.send(body);
  });
});
//byId
function getUserById (req, res){
  let id = req.params.id;
  let queryString='q={"_id":{"$oid":"'+ id+'"}}';
  httpClient.get(mLab+'user?'+queryString+apiKey,function(err,respuestaMLab, body){
    res.send(body[0]);
  });
}
app.get(URLbase+'users/:id',function(req,res){
  getUserById (req, res);
});

//byEmail
function updateSession (req, resp,status){
  let email = req.body.email;
  let password = req.body.password;
  let queryString='q={"email":"'+ email+'"}';

  httpClient.get(mLab+'user?'+queryString+apiKey,function(err,respuestaMLab, body){
 
    var userData = body[0];

       
    resp.send(userData);

 
    
  });

 
}
//Login
app.post(URLbase+'login', function(req, res) {
   updateSession(req, res, true);
});

app.post(URLbase+'logout', function(req, res) {
  updateSession(req, res, true);
});

//Logout
//Users
//Accounts
app.get(URLbase+'accounts',function(req, res){
  httpClient.get(mLab+'account?'+apiKey,function(err,respuestaMLab, body){
    res.send(body);
  });
});
//User Accounts
app.get(URLbase+'accounts/users/:id',function(req, res){
  let id = req.params.id;
  let queryString='q={"idusuario":"'+ id+'"}';
  httpClient.get(mLab+'account?'+queryString+apiKey,function(err,respuestaMLab, body){
    res.send(body);
  });
  
});
//Account C
app.post(URLbase+'accounts', function(req, res) {  
  let accountData = {
        "IBAN": req.body.IBAN,
        "idusuario": req.body.idusuario,
        "balance": req.body.balance
    }
  
    httpClient.post(mLab + 'account?' + apiKey, accountData, function(err, res, body){});
    res.statusCode = 200 ? res.send({"msj":"OK","code":"200"}) : res.send({"msj":"FAILED","code":"404"});
});

//Movements
app.post(URLbase+'movements', function(req, res) {  
  let movementData = 
    {
      "fecha": req.bodyfecha, //mda
      "descripcion": req.body.descripcion,
      "importe": req.body.importe,
      "IBAN": req.body.IBAN,
      "product":req.body.product,
      "lat":req.body.latitude,
      "lon":req.body.logitude,
      "time":req.body.time

    }
    httpClient.post(mLab + 'movement?' + apiKey, movementData, function(err, res, body){});
    res.statusCode = 200 ? res.send({"msj":"OK","code":"200"}) : res.send({"msj":"FAILED","code":"404"});
});
app.get(URLbase+'movements',function(req, res){
  httpClient.get(mLab+'movement?'+apiKey,function(err,respuestaMLab, body){
    res.send(body);
  });
}); 
//Movements By Account 
app.get(URLbase+'movements/:id',function(req, res){
  let id = req.params.id;
  let queryString='q={"product":"'+ id+'"}';
  httpClient.get(mLab+'movement?'+queryString+apiKey,function(err,respuestaMLab, body){
    res.send(body);
  });
}); 


